-- Database: beeschool

-- DROP DATABASE beeschool;

CREATE DATABASE beeschool
  WITH OWNER = beeschool
       ENCODING = 'LATIN1'
       TABLESPACE = beeschool
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;
GRANT CONNECT, TEMPORARY ON DATABASE beeschool TO public;
GRANT ALL ON DATABASE beeschool TO beeschool;

COMMENT ON DATABASE beeschool
  IS 'This database is for specific application access with less priviligies';



-- Table: assignment

-- DROP TABLE assignment;

CREATE TABLE assignment
(
  ve_id numeric(8,0) NOT NULL,
  pe_id numeric(20,0) NOT NULL,
  as_start date NOT NULL,
  as_end date NOT NULL,
  CONSTRAINT pk_assignment PRIMARY KEY (pe_id, ve_id),
  CONSTRAINT fk_assignme_drives_person FOREIGN KEY (pe_id)
      REFERENCES person (pe_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_assignme_schedules_vehicle FOREIGN KEY (ve_id)
      REFERENCES vehicle (ve_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE assignment
  OWNER TO postgres;
GRANT ALL ON TABLE assignment TO postgres;
GRANT ALL ON TABLE assignment TO beeschool;

-- Index: assignment_pk

-- DROP INDEX assignment_pk;

CREATE UNIQUE INDEX assignment_pk
  ON assignment
  USING btree
  (pe_id, ve_id);

-- Index: drives_fk

-- DROP INDEX drives_fk;

CREATE INDEX drives_fk
  ON assignment
  USING btree
  (pe_id);

-- Index: schedules_fk

-- DROP INDEX schedules_fk;

CREATE INDEX schedules_fk
  ON assignment
  USING btree
  (ve_id);

-- Table: licence

-- DROP TABLE licence;

CREATE TABLE licence
(
  sc_id numeric(8,0) NOT NULL,
  mo_id numeric(2,0) NOT NULL,
  sm_start_lic date NOT NULL,
  sm_end_lic date NOT NULL,
  CONSTRAINT pk_licence PRIMARY KEY (mo_id, sc_id),
  CONSTRAINT fk_licence_acquires_school FOREIGN KEY (sc_id)
      REFERENCES school (sc_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_licence_serves_module FOREIGN KEY (mo_id)
      REFERENCES module (mo_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE licence
  OWNER TO postgres;
GRANT ALL ON TABLE licence TO postgres;
GRANT ALL ON TABLE licence TO beeschool;

-- Index: acquires_fk

-- DROP INDEX acquires_fk;

CREATE INDEX acquires_fk
  ON licence
  USING btree
  (sc_id);

-- Index: licence_pk

-- DROP INDEX licence_pk;

CREATE UNIQUE INDEX licence_pk
  ON licence
  USING btree
  (mo_id, sc_id);

-- Index: serves_fk

-- DROP INDEX serves_fk;

CREATE INDEX serves_fk
  ON licence
  USING btree
  (mo_id);


-- Table: loc_class

-- DROP TABLE loc_class;

CREATE TABLE loc_class
(

)
WITH (
  OIDS=FALSE
);
ALTER TABLE loc_class
  OWNER TO postgres;
GRANT ALL ON TABLE loc_class TO postgres;
GRANT ALL ON TABLE loc_class TO beeschool;


-- Table: location

-- DROP TABLE location;

CREATE TABLE location
(
  lo_id numeric(10,0) NOT NULL,
  lo_id_parent numeric(10,0),
  lc_id numeric(2,0) NOT NULL,
  lo_name character varying(60) NOT NULL,
  CONSTRAINT pk_location PRIMARY KEY (lo_id),
  CONSTRAINT fk_location_incorpora_location FOREIGN KEY (lo_id_parent)
      REFERENCES location (lo_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE location
  OWNER TO postgres;
GRANT ALL ON TABLE location TO postgres;
GRANT ALL ON TABLE location TO beeschool;

-- Index: holds_fk

-- DROP INDEX holds_fk;

CREATE INDEX holds_fk
  ON location
  USING btree
  (lc_id);

-- Index: incorporates_fk

-- DROP INDEX incorporates_fk;

CREATE INDEX incorporates_fk
  ON location
  USING btree
  (lo_id_parent);

-- Index: location_pk

-- DROP INDEX location_pk;

CREATE UNIQUE INDEX location_pk
  ON location
  USING btree
  (lo_id);



-- Table: module

-- DROP TABLE module;

CREATE TABLE module
(
  mo_id numeric(2,0) NOT NULL,
  mo_name character varying(10) NOT NULL,
  mo_desc character varying(50) NOT NULL,
  CONSTRAINT pk_module PRIMARY KEY (mo_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE module
  OWNER TO postgres;
GRANT ALL ON TABLE module TO postgres;
GRANT ALL ON TABLE module TO beeschool;

-- Index: module_pk

-- DROP INDEX module_pk;

CREATE UNIQUE INDEX module_pk
  ON module
  USING btree
  (mo_id);
  
-- Table: notifier

-- DROP TABLE notifier;

CREATE TABLE notifier
(
  no_id numeric(24,0) NOT NULL,
  pe_id numeric(20,0) NOT NULL,
  no_date_event date NOT NULL,
  no_status numeric(1,0) NOT NULL,
  no_description character varying(100) NOT NULL,
  CONSTRAINT pk_notifier PRIMARY KEY (no_id),
  CONSTRAINT fk_notifier_reports_person FOREIGN KEY (pe_id)
      REFERENCES person (pe_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifier
  OWNER TO postgres;
GRANT ALL ON TABLE notifier TO postgres;
GRANT ALL ON TABLE notifier TO beeschool;

-- Index: notifier_pk

-- DROP INDEX notifier_pk;

CREATE UNIQUE INDEX notifier_pk
  ON notifier
  USING btree
  (no_id);

-- Index: reports_fk

-- DROP INDEX reports_fk;

CREATE INDEX reports_fk
  ON notifier
  USING btree
  (pe_id);

-- Table: office

-- DROP TABLE office;

CREATE TABLE office
(
  lo_id numeric(10,0) NOT NULL,
  sc_id numeric(8,0) NOT NULL,
  of_number numeric(3,0) NOT NULL,
  CONSTRAINT pk_office PRIMARY KEY (sc_id, lo_id),
  CONSTRAINT fk_office_hosts_location FOREIGN KEY (lo_id)
      REFERENCES location (lo_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_office_maintains_school FOREIGN KEY (sc_id)
      REFERENCES school (sc_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE office
  OWNER TO postgres;
GRANT ALL ON TABLE office TO postgres;
GRANT ALL ON TABLE office TO beeschool;

-- Index: hosts_fk

-- DROP INDEX hosts_fk;

CREATE INDEX hosts_fk
  ON office
  USING btree
  (lo_id);

-- Index: maintains_fk

-- DROP INDEX maintains_fk;

CREATE INDEX maintains_fk
  ON office
  USING btree
  (sc_id);

-- Index: office_pk

-- DROP INDEX office_pk;

CREATE UNIQUE INDEX office_pk
  ON office
  USING btree
  (sc_id, lo_id);

-- Table: parent

-- DROP TABLE parent;

CREATE TABLE parent
(
  st_id numeric(20,0) NOT NULL,
  pe_id numeric(20,0) NOT NULL,
  pa_start_lic date NOT NULL,
  pa_end_lic date NOT NULL,
  CONSTRAINT pk_parent PRIMARY KEY (pe_id, st_id),
  CONSTRAINT fk_parent_cares_person FOREIGN KEY (pe_id)
      REFERENCES person (pe_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_parent_relates_student FOREIGN KEY (st_id)
      REFERENCES student (st_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE parent
  OWNER TO postgres;
GRANT ALL ON TABLE parent TO postgres;
GRANT ALL ON TABLE parent TO beeschool;

-- Index: cares_fk

-- DROP INDEX cares_fk;

CREATE INDEX cares_fk
  ON parent
  USING btree
  (pe_id);

-- Index: parent_pk

-- DROP INDEX parent_pk;

CREATE UNIQUE INDEX parent_pk
  ON parent
  USING btree
  (pe_id, st_id);

-- Index: relates_fk

-- DROP INDEX relates_fk;

CREATE INDEX relates_fk
  ON parent
  USING btree
  (st_id);

-- Table: person

-- DROP TABLE person;

CREATE TABLE person
(
  pe_id numeric(20,0) NOT NULL,
  sc_id numeric(8,0) NOT NULL,
  ty_id numeric(2,0),
  pe_name character varying(30) NOT NULL,
  pe_last_name character varying(30) NOT NULL,
  pe_sex character(1) NOT NULL,
  pe_password character varying(100) NOT NULL,
  pe_e_mail character varying(100) NOT NULL,
  CONSTRAINT pk_person PRIMARY KEY (pe_id),
  CONSTRAINT fk_person_employs_school FOREIGN KEY (sc_id)
      REFERENCES school (sc_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_person_subtypes_types FOREIGN KEY (ty_id)
      REFERENCES types (ty_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE person
  OWNER TO postgres;
GRANT ALL ON TABLE person TO postgres;
GRANT ALL ON TABLE person TO beeschool;

-- Index: employs_fk

-- DROP INDEX employs_fk;

CREATE INDEX employs_fk
  ON person
  USING btree
  (sc_id);

-- Index: person_pk

-- DROP INDEX person_pk;

CREATE UNIQUE INDEX person_pk
  ON person
  USING btree
  (pe_id);

-- Index: subtypes_fk

-- DROP INDEX subtypes_fk;

CREATE INDEX subtypes_fk
  ON person
  USING btree
  (ty_id);

-- Table: route

-- DROP TABLE route;

CREATE TABLE route
(
  pe_id_co numeric(20,0) NOT NULL,
  pe_id_dr numeric(20,0) NOT NULL,
  st_id numeric(20,0) NOT NULL,
  ro_stop numeric(3,0) NOT NULL,
  ro_date timestamp without time zone NOT NULL,
  ro_new character varying(100),
  ro_latitude numeric(10,8) NOT NULL,
  ro_longitude numeric(10,8) NOT NULL,
  CONSTRAINT route_pkey PRIMARY KEY (pe_id_co, pe_id_dr, st_id, ro_date),
  CONSTRAINT fk_route_controls_person FOREIGN KEY (pe_id_dr)
      REFERENCES person (pe_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_route_runs_person FOREIGN KEY (pe_id_co)
      REFERENCES person (pe_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_route_uses_student FOREIGN KEY (st_id)
      REFERENCES student (st_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE route
  OWNER TO postgres;
GRANT ALL ON TABLE route TO postgres;
GRANT ALL ON TABLE route TO beeschool;

-- Index: controls_fk

-- DROP INDEX controls_fk;

CREATE INDEX controls_fk
  ON route
  USING btree
  (pe_id_dr);

-- Index: route_pk

-- DROP INDEX route_pk;

CREATE UNIQUE INDEX route_pk
  ON route
  USING btree
  (pe_id_co, pe_id_dr, st_id, ro_date);

-- Index: uses_fk

-- DROP INDEX uses_fk;

CREATE INDEX uses_fk
  ON route
  USING btree
  (st_id);

-- Table: school

-- DROP TABLE school;

CREATE TABLE school
(
  sc_id numeric(8,0) NOT NULL,
  sc_name character varying(100) NOT NULL,
  sc_address character varying(60) NOT NULL,
  CONSTRAINT pk_school PRIMARY KEY (sc_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE school
  OWNER TO postgres;
GRANT ALL ON TABLE school TO postgres;
GRANT ALL ON TABLE school TO beeschool;

-- Index: school_pk

-- DROP INDEX school_pk;

CREATE UNIQUE INDEX school_pk
  ON school
  USING btree
  (sc_id);

-- Table: student

-- DROP TABLE student;

CREATE TABLE student
(
  st_id numeric(20,0) NOT NULL,
  sc_id numeric(8,0) NOT NULL,
  st_name character varying(30) NOT NULL,
  st_last_name character varying(30) NOT NULL,
  st_grade numeric(2,0) NOT NULL,
  st_status numeric(1,0) NOT NULL,
  st_sex character(1) NOT NULL,
  st_address character varying(60) NOT NULL,
  st_license numeric(3,0) NOT NULL,
  CONSTRAINT pk_student PRIMARY KEY (st_id),
  CONSTRAINT fk_student_educates_school FOREIGN KEY (sc_id)
      REFERENCES school (sc_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE student
  OWNER TO postgres;
GRANT ALL ON TABLE student TO postgres;
GRANT ALL ON TABLE student TO beeschool;

-- Index: educates_fk

-- DROP INDEX educates_fk;

CREATE INDEX educates_fk
  ON student
  USING btree
  (sc_id);

-- Index: student_pk

-- DROP INDEX student_pk;

CREATE UNIQUE INDEX student_pk
  ON student
  USING btree
  (st_id);

-- Table: types

-- DROP TABLE types;

CREATE TABLE types
(
  ty_id numeric(2,0) NOT NULL,
  ty_name character varying(20) NOT NULL,
  ty_description character varying(100),
  CONSTRAINT pk_types PRIMARY KEY (ty_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE types
  OWNER TO postgres;
GRANT ALL ON TABLE types TO postgres;
GRANT ALL ON TABLE types TO beeschool;

-- Index: types_pk

-- DROP INDEX types_pk;

CREATE UNIQUE INDEX types_pk
  ON types
  USING btree
  (ty_id);

-- Table: vehicle

-- DROP TABLE vehicle;

CREATE TABLE vehicle
(
  ve_id numeric(8,0) NOT NULL,
  ve_quota numeric(3,0) NOT NULL,
  CONSTRAINT pk_vehicle PRIMARY KEY (ve_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vehicle
  OWNER TO postgres;
GRANT ALL ON TABLE vehicle TO postgres;
GRANT ALL ON TABLE vehicle TO beeschool;

-- Index: vehicle_pk

-- DROP INDEX vehicle_pk;

CREATE UNIQUE INDEX vehicle_pk
  ON vehicle
  USING btree
  (ve_id);




